module gitea.com/tango/cache-memcache

go 1.14

require (
	gitea.com/lunny/tango v0.6.2
	gitea.com/tango/cache v0.0.0-20200330032101-c35235184e65
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b
	github.com/smartystreets/goconvey v1.6.4
	github.com/unknwon/com v1.0.1
)
